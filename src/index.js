import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TopHeader from './Top Header/topheader';
import ButtonTriggerModal from './Button trigger modal/buttonTriggerModal';
import Login from './Login/login';
import Register from './Register/register';
import HeaderBottom from './Header Bottom/headerBottom';
import Navigation from './Navigation/navigation';
import Banner from './Banner/banner';
import Product from './Product/product';
import MiddleSession from './Middle Session/middleSession';
import Footer from './Footer/footer';
import CopyRight from './Copy Right/copyRight';
import * as serviceWorker from './serviceWorker';

// ReactDOM.render(<TopHeader />, document.getElementById('topHeader'));
ReactDOM.render(<ButtonTriggerModal />, document.getElementById('buttonTriggerModal'));
ReactDOM.render(<Login />, document.getElementById('login'));
ReactDOM.render(<Register />, document.getElementById('register'));
ReactDOM.render(<HeaderBottom />, document.getElementById('headerBottom'));
ReactDOM.render(<Navigation />, document.getElementById('navigation'));
ReactDOM.render(<Banner />, document.getElementById('banner'));
ReactDOM.render(<Product />, document.getElementById('product'));
ReactDOM.render(<MiddleSession />, document.getElementById('middleSession'));
ReactDOM.render(<Footer />, document.getElementById('footer'));
ReactDOM.render(<CopyRight />, document.getElementById('copyRight'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
