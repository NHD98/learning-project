import React from 'react';

function CopyRight() {
  return (
    <div className="copy-right py-3">
      <div className="container">
        <p className="text-center text-white">© 2018 Electro Store. All rights reserved | Design by
				<a href="http://w3layouts.com"> W3layouts.</a>
        </p>
      </div>
    </div>);
}

export default CopyRight;
